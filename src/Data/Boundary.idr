||| A boundary is a pair of types, they are the object of the category of lenses
module Data.Boundary

%default total

||| Type boundaries
public export
record Boundary where
  constructor MkB
  proj1 : Type
  proj2 : Type

||| First projection of a boundary
public export
(.π1) : Boundary -> Type
(.π1) = proj1

||| Second projection of a boundary
public export
(.π2) : Boundary -> Type
(.π2) = proj2

||| Cartesian product of two boundaries
||| cartesian (a , b) (c, d) = (a * c, b * d)
public export
cartesian : Boundary -> Boundary -> Boundary
cartesian a b = (MkB (a.proj1, b.proj1) (a.proj2, b.proj2))

||| Build a boundary out of a single type
public export
Dup : Type -> Boundary
Dup ty = MkB ty ty

||| The unit boundary is a pair of units
public export
BUnit : Boundary
BUnit = MkB Unit Unit

||| Cocartesian product of boundaries
||| cocartesian (a, b) (c, d) = (a + c, b + d)
public export
cocartesian : Boundary -> Boundary -> Boundary
cocartesian a b = MkB (Either a.proj1 b.proj1) (Either a.proj2 b.proj2)
