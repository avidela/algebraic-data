module Data.Coproduct

import Control.Function

import Data.List
import Data.Fin
import public Data.Ops

%default total

||| co-products
public export
data (+) : Type -> Type -> Type where
  ||| Left choice
  (<+) : a -> a + b
  ||| Right choice
  (+>) : b -> a + b

||| Eliminator for co-products
public export
choice : (a -> c) -> (b -> c) -> a + b -> c
choice f g (<+ x) = f x
choice f g (+> x) = g x

public export
dchoice : {0 a, b : Type} -> {0 m : a + b -> Type} ->
          (l : (x : a) -> m (<+ x)) ->
          (r : (x : b) -> m (+> x)) ->
          (e : a + b) -> m e
dchoice l r (<+ x) = l x
dchoice l r (+> x) = r x

public export
dia : a + a -> a
dia (<+ x) = x
dia (+> x) = x

||| Co-product is a bifunctor
public export
Bifunctor (+) where
  bimap f g (<+ x) = <+ (f x)
  bimap f g (+> x) = +> (g x)

export
Show a => Show b => Show (a + b) where
  show (<+ l) = show l
  show (+> r) = show r

||| Left choice is injective
export
Injective (<+) where
  injective Refl = Refl

||| Right choice is injective
export
Injective (+>) where
  injective Refl = Refl

||| A sum of `n` values of type `a`
public export
Sum : (n : Nat) -> (a : Type) -> Type
Sum Z y = Void
Sum (S Z) y = y
Sum (S x) y = y + Sum x y

public export
SumList : List Type -> Type
SumList [] = Void
SumList (x :: xs) = x + SumList xs

public export
match : {a : _} -> SumList a -> SumList b -> SumList (a ++ b)
match {a = []} x y = y
match {a = (ty :: xs)} (<+ x) y = <+ x
match {a = (ty :: xs)} (+> x) y = +> match x y

export
split : {a : _} -> SumList (a ++ b) -> SumList a + SumList b
split {a = []} x = +> x
split {a = (y :: xs)} (<+ x) = <+ <+ x
split {a = (y :: xs)} (+> x) = case split x of
                                    (<+ z) => <+ (+> z)
                                    (+> z) => +> z

||| A selector is a pair of a fin n and a value of type a the Fin allows to
||| pick where in a sum of `n` values of `a` the value fit.
public export
Selector : Nat -> Type -> Type
Selector n a = (Fin n, a)

||| Convert from a selector to a sum of a
public export
finToSum : {0 n : Nat} -> Selector n a -> Sum n a
finToSum {n = S Z} (FZ, y) = y
finToSum {n = S Z} (FS x, y) = y

||| Diagonal operator on a sum of `n` values of `a`
export
diagonal : (n : Nat) -> Sum n a -> a
diagonal 0 x = void x
diagonal (S 0) x = x
diagonal (S (S k)) (<+ x) = x
diagonal (S (S k)) (+> x) = diagonal (S k) x

||| Convert a sum of n values of a into a pair that records which values of `a` was in the sum in a fin
export
sumToSelector : {n : Nat} -> Sum n a -> (Fin n, a)
sumToSelector x {n = Z} = void x
sumToSelector x {n = (S Z)} = (FZ, x)
sumToSelector (<+ x) {n = (S (S k))} = (FZ, x)
sumToSelector (+> x) {n = (S (S k))} = mapFst FS $ sumToSelector {n = S k} x

||| A dependent choice from `n` values of `a`
public export
ChoiceN : (n : Nat) -> (ty : a -> Type) -> Sum n a -> Type
ChoiceN Z ty _ = Void
ChoiceN (S Z) ty v = ty v
ChoiceN (S (S n)) ty idx =
  Coproduct.choice (ty) (\x => ChoiceN (S n) ty x) idx

export
diaChoice : {n : Nat} -> {0 b : Type} ->
            (c : Sum n a) -> b -> ChoiceN n {a} (const b) c
diaChoice {n = 0} c x = c
diaChoice {n = (S 0)} c x = x
diaChoice {n = (S (S k))} (<+ y) x = x
diaChoice {n = (S (S k))} (+> y) x = diaChoice y x

public export
fromMaybe : Maybe a -> () + a
fromMaybe = maybe (<+ ()) (+>)

public export
swap : a + b -> b + a
swap (<+ x) = +> x
swap (+> x) = <+ x

||| Extract the left value
public export
alwaysLeft : a + Void -> a
alwaysLeft (<+ x) = x

||| Extract the right value
public export
alwaysRight : Void + b -> b
alwaysRight (+> x) = x

