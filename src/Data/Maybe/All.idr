module Data.Maybe.All

public export
data All : (a -> Type) -> Maybe a -> Type where
  Nil : All p Nothing
  Value : {0 x : a} -> p x -> All p (Just x)

export
(.value) : {0 a : Type} -> {0 p : a -> Type} -> {0 x : a} -> All p (Just x) -> p x
(.value) (Value y) = y

export
app : {0 a, b : Type} -> {0 x : a} -> {0 p : a -> Type} -> {0 y : b} -> {0 q : b -> Type} ->
      (p x -> q y) -> All p (Just x) -> All q (Just y)
app f = Value . f . (.value)

export
fromMaybe : {0 a : Type} -> {0 p : a -> Type} ->
            ((x : a) -> p x) -> (m : Maybe a) -> All p m
fromMaybe f Nothing = []
fromMaybe f (Just x) = Value (f x)

