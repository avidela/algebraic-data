module Data.Maybe.Any

import Data.DPair

public export
data Any : (a -> Type) -> Maybe a -> Type where
  Here : {0 a : Type} -> {0 v : a} -> {0 p : a -> Type} -> p v -> Any p (Just v)

public export
Uninhabited (Any p Nothing) where
  uninhabited _ impossible

public export
extract : Any p (Just x) -> p x
extract (Here y) = y

public export
extractErased : {0 x : Maybe a} -> Any p x -> Exists p
extractErased (Here y) = Evidence _ y

public export
extractValue : {x : Maybe a} -> Any p x -> (v : a ** p v)
extractValue {x = Just v} (Here y) = (v ** y)

export
app : {0 a, b : Type} -> {0 x : a} -> {0 p : a -> Type} -> {0 y : b} -> {0 q : b -> Type} ->
      (p x -> q y) -> Any p (Just x) -> Any q (Just y)
app f = Here . f . extract

