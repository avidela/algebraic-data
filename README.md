# Algebraic data structures

`algdata` exports generic data definitions using operators product, co-products and tensor.

- `Data.Product` exports `*` for pairs.
- `Data.Coproduct` for `+` for "either".
- `Data.Sigma` exports `Σ` for sigma-types.
- `Data.Boundary` exports boundaries, objects of the category of lenses
- `Data.Alg` exports algebraic properties of data types
